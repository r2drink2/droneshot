from synapse.platforms import *

buttonPin = 15 #DIO4
icPin = 12 #CTS

TRANSMITTER_NODE = "\x5D\x26\x4E"


@setHook(HOOK_STARTUP)
def startupEvent():
    setPinDir(icPin, True)
    writePin(icPin, True)
    setPinDir(buttonPin, False)
    setPinPullup(buttonPin, True)
    monitorPin(buttonPin, True)

@setHook(HOOK_GPIN)
def getInput(pinNumber, newValue):
    if pinNumber == buttonPin and newValue == 0:
        rpc(TRANSMITTER_NODE, "push_button");
        pulsePin(icPin, 100, False)
    
    

    
