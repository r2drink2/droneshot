from synapse.platforms import *

buttonPin = 15 #DIO4
blinkPin = 12 #CTS


@setHook(HOOK_STARTUP)
def startupEvent():
    setPinDir(blinkPin, True)
    writePin(blinkPin, False)
    setPinDir(buttonPin, True)
    writePin(buttonPin, False)


def push_button():
    pulsePin(buttonPin, 100, True)
    pulsePin(blinkPin, 500, True)
