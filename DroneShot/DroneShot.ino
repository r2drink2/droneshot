// Arduino Leonardo for JTron beetle
// Arduino Pro Mini 5V 16MHz otherwise
#include <Servo.h>


const int LED_PIN = 13;

const int GLASS_SERVO_PIN = 9;
Servo GLASS_SERVO;
const int GLASS_SERVO_CARRY_POS = 1550;
const int GLASS_SERVO_POUR_POS = 600;


/* Official WLToys camera connector pinout:
red: +5v
black: gnd
yellow: +3.3v nominal, pull to ground for 250ms to start or stop video fuction (top left edge TX button)
white: +3.3v nominal, pull to ground for 250ms to take a still photograph (bottom right menu TX button)
*/


/* Unoffical bluetooth serial cable pinout:
VCC = Green
GND = Yellow
VIDEO_IN_PIN = Red
STILL_IN_PIN = Black
*/

const int VIDEO_IN_PIN = 11;
const int STILL_IN_PIN = 10;


void setup() {

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  pinMode(GLASS_SERVO_PIN, OUTPUT);
  digitalWrite(GLASS_SERVO_PIN, LOW);

  pinMode(VIDEO_IN_PIN, INPUT);
  pinMode(STILL_IN_PIN, INPUT);


  GLASS_SERVO.writeMicroseconds(GLASS_SERVO_CARRY_POS);
  GLASS_SERVO.attach(GLASS_SERVO_PIN);

  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
  delay(500);
}


void loop() {
  if(digitalRead(VIDEO_IN_PIN) == LOW || digitalRead(STILL_IN_PIN) == LOW) {
    for(int i=0; i<10; i++) {
      int servo_pos = (GLASS_SERVO_CARRY_POS - GLASS_SERVO_POUR_POS);
      servo_pos = servo_pos / 10;
      servo_pos = servo_pos * i;
      servo_pos = GLASS_SERVO_CARRY_POS - servo_pos;
      GLASS_SERVO.writeMicroseconds(servo_pos);
      delay(100);
    }
    GLASS_SERVO.writeMicroseconds(GLASS_SERVO_POUR_POS);
    delay(2000);
    GLASS_SERVO.writeMicroseconds(GLASS_SERVO_CARRY_POS);
    delay(1000);
  }
}


